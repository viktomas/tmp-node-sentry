
const Sentry = require("@sentry/node");

Sentry.init({
  dsn: "https://c6c2e95dcb7e9846528918731bbef89b@new-sentry.gitlab.net/33",

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
  debug: true,
});

Sentry.startSpan({
  op: "test",
  name: "My First Test Transaction",
}, () => {
  setTimeout(() => {
    try {
      throw new Error('test-error');
    } catch (e) {
      Sentry.captureException(e);
    }
  }, 99);
});
